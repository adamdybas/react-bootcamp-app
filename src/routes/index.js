import MovieList from '../components/MoviesList';
import MovieDetails from '../components/MovieDetails';
import BookingList from '../components/BookingList';

export default {
  routes: [
    {
      id: 1,
      path: '/movies',
      component: MovieList,
      exact: true,
    },
    {
      id: 2,
      path: '/movie/:id',
      component: MovieDetails,
      exact: true,
    },
    {
      id: 3,
      path: '/bookings/:userId',
      component: BookingList,
      exact: true,
    },
  ],
  redirects: [
    {
      id: 1,
      from: '/',
      to: '/movies',
      exact: true,
    },
    {
      id: 2,
      from: '/redirect',
      to: '/movies',
    },
  ]
}
