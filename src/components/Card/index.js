import React from 'react';
import PropTypes from 'prop-types';
import RunningTime from '../RunningTime';
import Alert from '../Alert';
import Counter from '../Counter';
import StarRates from '../StarRates';
import styles from './Card.module.scss';

const Card = ({title, duration, img, desc, soldedOut, rating, alert}) => {
  return (
    <div className={styles.card}>
        <div className={styles.backgroundImg}>
          <img src={img} alt="" />
        </div>
        <div className={styles.cardData}>
          <div className={styles.cardText}>
            <header>
              <h3>
                {title || 'Brak tytułu'}
              </h3>
              <div className={styles.startTime}>
                Premiera: <time>18.10.2018</time>
              </div>
              <RunningTime duration={duration} />
            </header>
            <p>{desc}</p>
          </div>
          <div className={styles.cardAdditional}>
            <Alert title={alert.message} type={alert.type} />
            <Counter />
            <StarRates rating={rating} />
          </div>
        </div>
    </div>
  )
};
Card.defaultProps = {
  title: 'Test2',
  desc: 'Brak opisu'
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  duration: PropTypes.number,
  image: PropTypes.string,
  soldedOut: PropTypes.bool
}
export default Card;
