import React from 'react';
import styles from './Alert.module.scss';

const iconType = {
  success: 'check-circle',
  error: 'exclamation-circle',
  warning: 'exclamation-triangle'
}

const Alert = ({title, type}) => {
  return (
    <div className={`${styles.container} ${styles[type]}`}>
      <img src={require(`../../images/${iconType[type]}.svg`)} alt="" className={styles.icon} />
      <div className={styles.title}>{title}</div>
    </div>
  )
};

export default Alert;
