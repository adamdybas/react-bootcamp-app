import React, {Component} from 'react';
import Card from '../Card';
import BookingForm from '../BookingForm';
import MovieFooter from '../MovieFooter';
import { NavLink } from 'react-router-dom';
import styles from './MovieDetails.module.scss'

const data = [
    {
        title: 'First Man',
        releaseDate: '18.10.2018',
        desc: 'Fragment życia astronauty Neila Armstronga i jego legendarnej misji kosmicznej, dzięki której jako pierwszy człowiek stanął na Księżycu.',
        duration: 8460,
        img: 'https://i.imgur.com/0oo7XJc.jpg',
        rating: 4,
        soldedOut: false,
        id: 1,
        alert: {
            message: 'Wyprzedane',
            type: 'error',
        }
    },
    {
        title: 'Mission: Impossible - Fallout',
        desc: 'Konsekwencje zakończonej niepowodzeniem misji IMF może odczuć cały świat. Aby zapobiec katastrofie, Ethan Hunt i jego zespół muszą stanąć do wyścigu z czasem.',
        duration: 8820,
        releaseDate: '09.09.2018',
        img: 'https://i.imgur.com/rOXaXH6.jpg',
        rating: 5,
        soldedOut: false,
        id: 2,
        alert: {
            message: 'Ostatnie miejsca',
            type: 'warning',
        }
    },
    {
        title: 'American Animals',
        releaseDate: '01.09.2018',
        desc: 'Wracając od kolegi, Will zauważa coś przerażającego. Pobliskie laboratorium rządowe skrywa złowrogą tajemnicę. Ogólnie jest nie za wesoło,' +
        ' ale wszystko kończy się dobrze i żyją długo i szczęśliwie.',
        duration: 3200,
        img: 'https://i.imgur.com/3koreob.jpg',
        rating: 3,
        soldedOut: true,
        id: 3,
        alert: {
            message: 'Nowość!',
            type: 'success',
        }
    },
];

const [{id, title, desc, duration, img, rating, alert}] = data;

const backBtn = (
  <NavLink to="/movies">
    Wróć
  </NavLink>
)

class MovieDetails extends Component {
  render() {
    return (
      <div className={styles.cardWrap}>
        <Card
          key={id}
          title={title}
          desc={desc}
          img={img}
          duration={duration}
          rating={rating}
          alert={alert}
        >
        </Card>
        <BookingForm />
        <MovieFooter
          component={backBtn}
        />
      </div>
    )
  }
}

export default MovieDetails;
