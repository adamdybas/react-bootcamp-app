import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Navigation.module.scss'

const Navigation = () => (
  <ul className={styles.mainNav}>
    <li>
      <NavLink to='/movies' activeClassName={styles.active}>Movies</NavLink>
    </li>
    <li>
      <NavLink to='/bookings/1' activeClassName={styles.active}>Bookings</NavLink>
    </li>
    <li>
      <NavLink to='/redirect' activeClassName={styles.active}>Coming Soon</NavLink>
    </li>
  </ul>
)

export default Navigation;
