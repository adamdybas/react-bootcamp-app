import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

const Button = ({ soldedOut, addToCart, id }) => {
  return (
    <div className={styles.btnWrapper}>
      {
        // props.soldedOut &&
        soldedOut ?
          <span>niedostępny</span>
          :
          <button className={styles.btn} onClick={() => addToCart(id)}>Kup bilet</button>
      }
    </div>
  )
}

Button.propTypes = {
  soldedOut: PropTypes.bool
}

export default Button;
