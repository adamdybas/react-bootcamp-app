import React, {Component} from 'react';
import Button from '../Button';
import Alert from '../Alert';
import styles from './BookingForm.module.scss';

const movies = [
  {
    time: '17:45',
    id: 1
  },
  {
    time: '19:50',
    id: 2
  },
  {
    time: '22:20',
    id: 3
  },
]

class BookingForm extends Component {
  state = {
    inputValue: '',
    currentId: 0,
    movieTime: null,
    error: null
  }
  onInputChanage = (e) => {
    this.setState({
        inputValue: e.target.value
    })
  }
  setHour = (id, time) => {
    this.setState({
      movieTime: time,
      currentId: id,
    })
  }
  addToCart = (id, time) => {
    if (!this.state.movieTime && !this.state.inputValue) {
      this.setState({
        error: 'Proszę wybrać ilość miejsc oraz czas seansu',
      })
      return;
    } else if (!this.state.inputValue) {
        this.setState({
          error: 'Proszę wybrać ilość miejsc',
        })
        return;
    } else if (!this.state.movieTime) {
        this.setState({
          error: 'Proszę wybrać czas seansu',
        })
        return;
    } else {
      console.log(`Kupiłeś ${this.state.inputValue} biletów na godzinę ${this.state.movieTime}`)
      this.setState({
        currentId: id,
        movieTime: time,
        inputValue: this.state.inputValue,
        error: null
      })
    }
  }
  render(){
    const { error } = this.state;
    return(
        <div>
          <div className={styles.footerContent}>
            <div className={styles.hoursWrap}>
              {
                movies.map((movie) => {
                  return (
                    <div className={this.state.currentId === (movie.id) ? styles.activeHour : styles.hour} onClick={(id, time) => {this.setHour(movie.id, movie.time)}} id={movie.id} key={movie.id}>{movie.time}</div>
                  )
                })
              }
            </div>
            <div className={styles.formPlaces}>
              <label htmlFor="places">Ilość miejsc:</label>
              <input
                id="places"
                type="number"
                value={this.state.inputValue}
                onChange={this.onInputChanage} />
            </div>
            <div>
              <Button addToCart={this.addToCart} />
            </div>
          </div>
        {error &&
          <Alert
            className={styles.alert}
            title={error}
            type="error"
            showIcon
          />
        }
      </div>
    )
  }
}

export default BookingForm;
