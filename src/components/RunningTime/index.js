import React from 'react';
import PropTypes from 'prop-types';
import styles from './RunningTime.module.scss';

const RunningTime = ({duration}) => {
  return (
    <div className={styles.runningTime}>
      Czas trawnia:  <time>{parseInt(duration / 60)} min</time>
    </div>
  )
};

RunningTime.propTypes = {
  duration: PropTypes.number
}

export default RunningTime;
