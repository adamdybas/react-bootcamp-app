import React, { Component } from 'react';
import Card from '../Card';
import styles from './BookingList.module.scss';

const data = [
  {
    title: 'Mission: Impossible - Fallout',
    desc: 'Konsekwencje zakończonej niepowodzeniem misji IMF może odczuć cały świat. Aby zapobiec katastrofie, Ethan Hunt i jego zespół muszą stanąć do wyścigu z czasem.',
    duration: 8820,
    releaseDate: '09.09.2018',
    img: 'https://i.imgur.com/rOXaXH6.jpg',
    rating: 5,
    soldedOut: false,
    id: 2,
    alert: {
      message: 'Ostatnie miejsca',
      type: 'warning',
    },
    reservedSeats: 15,
    bookingTime: '17:30'
  },
  {
    title: 'American Animals',
    releaseDate: '01.09.2018',
    desc: 'Wracając od kolegi, Will zauważa coś przerażającego. Pobliskie laboratorium rządowe skrywa złowrogą tajemnicę. Ogólnie jest nie za wesoło,' +
    ' ale wszystko kończy się dobrze i żyją długo i szczęśliwie.',
    duration: 3200,
    img: 'https://i.imgur.com/3koreob.jpg',
    rating: 3,
    soldedOut: true,
    id: 3,
    alert: {
      message: 'Nowość!',
      type: 'success',
    },
    reservedSeats: 4,
    bookingTime: '12:30'
  },
];

class BookingList extends Component {
  handleDelete = (id) => {
    console.log('deleted: ', id);
  }
  render() {
    return (
      <div>
        {
          data.map((movie) => {
            return (
            <div className={styles.cardWrap} key={movie.id}>
              <Card
                title={movie.title}
                desc={movie.desc}
                img={movie.img}
                duration={movie.duration}
                soldedOut={movie.soldedOut}
                alert={movie.alert}
                rating={movie.rating}
              />
            <div className={styles.bookingInfo}>
              <p><strong>Ilość biletów:</strong> {movie.reservedSeats}</p>
              <p><strong>Godzina rezerwacji:</strong> {movie.bookingTime}</p>
            </div>
            <button
              onClick={() => this.handleDelete(movie['id'])}
              type="danger"
              className={styles.btn}
              >
                Usuń
            </button>
            </div>
          )
        })
      }
    </div>
    )
  }
}

export default BookingList;
