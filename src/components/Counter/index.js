import React, { Component } from 'react';
import styles from './Counter.module.scss';

class Counter extends Component {
  state = {
    value: 0
  }
  changeValue = (operation) => {
    if(this.state.value + operation === -1) {
      this.setState((prevState) => {
        return {
          value: this.state.value
        }
      });
    } else {
      this.setState((prevState) => {
        return {
          value: prevState.value + operation
        }
      });
    }
  }
  render() {
    return(
      <div className={styles.counter}>
        <button onClick={() => this.changeValue(1)}>Tak</button>
        <span>
          {this.state.value}
        </span>
        <button onClick={() => this.changeValue(-1)}>Nie</button>
      </div>
    )
  }
}

export default Counter;
