import React, { Component } from 'react';
import styles from './StarRates.module.scss';

const ratingDesc = ['Dno', 'Ujdzie', 'Zjadliwy', 'Dobry', 'Arcydzieło'];

class StarRates extends Component {
  state = {
    value: this.props.rating,
    message: ratingDesc[this.props.rating - 1]
  }
  changeRate = (index) => {
    this.setState({
      value: index + 1
    });
    this.setState({
      message: ratingDesc[index]
    });
  }
  render() {
    return(
      <div>
        {
          ratingDesc.map((item, index) =>
          <div className={this.state.value >= (index + 1) ? styles.star : styles.starEmpty} onClick={() => this.changeRate(index)} value={item} key={index} rating={index + 1}>{index + 1}</div>)
        }
        <span>{this.state.message}</span>
      </div>
    )
  }
};

export default StarRates;
